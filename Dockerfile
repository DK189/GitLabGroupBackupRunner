FROM php:8.1.0-cli

RUN apt-get update && apt-get install -y git

WORKDIR /workspace/

RUN ls

WORKDIR /app/

COPY runner.php runner.php

ENTRYPOINT ["php", "runner.php"]