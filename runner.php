<?php

echo "Sync job was registered!\n";

$cfgDelay = isset($_ENV["SECONDS_DELAY_BEFORE_RUN"]) ? $_ENV["SECONDS_DELAY_BEFORE_RUN"] : "0";


try {
    $cfgDelay = intval($cfgDelay);
} catch (\Throwable $th) {
    $cfgDelay = 0;
}

while ($cfgDelay > 0) {
    echo "\rwaiting ${cfgDelay}s before start...\r";
    
    sleep(1);

    $cfgDelay--;
}
echo "\r                                                                \n";

echo "Starting sync tool...\n.\n.\n.\n\n";

$cfgApiEndpoint = $_ENV["API_ENDPOINT"];
$cfgGitUsername = $_ENV["GIT_USERNAME"];
$cfgGitToken = $_ENV["GIT_TOKEN"];

$apiLink = "$cfgApiEndpoint";

$queryObject = [
    "include_subgroups"=>"yes",
    "private_token"=>"glpat-iGiwqdyj-YsV91x5FtCE",
    "per_page"=>100,
    "page"=>0,
];

$lastCount = 0;

$all = [];

do {
    $lastCount = 0;
    try {
        $queryObject["page"]++;
        $uri = $apiLink . "?" . http_build_query($queryObject);

        echo "\r api $uri: fetching...";

        $content = file_get_contents($uri);

        if ($content && !empty("$content")) {
            $data = json_decode($content);

            if ($data && is_array($data)) {
                $lastCount = count($data);
                echo "\r api $uri: fetched $lastCount repositories.\n";
                $all = array_merge($all, $data);
            }
        }
    } catch (\Exception $ex) {
        $lastCount = 0;
        var_dump($ex);
    }
} while ($lastCount == 100);

$allCount = count($all);

echo "found $allCount repositories in group.\n\n";

foreach($all as $index=>$repo) {

    $no = $index + 1;

    $repoGit = $repo->http_url_to_repo;
    $repoPath = $repo->path_with_namespace;

    echo "[ $no / $allCount ] start sync for: $repoPath \n";

    $_repoGit = parse_url($repoGit);

    $repoGit = $_repoGit["scheme"] . "://" . "$cfgGitUsername:$cfgGitToken" . "@" . $_repoGit["host"] . $_repoGit["path"];

    $localPath = "/workspace/" . $repoPath;

    var_dump(shell_exec("if [ -d " . $localPath . " ]; then git -C " . $localPath . " pull --ff-only; else git clone " . $repoGit . " " . $localPath . "; fi;"));


    echo "[DONE]\n\n";
}