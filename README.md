# GitLabGroupBackupRunner


### Configuration

add file ```.env``` with group information like here

```env
API_ENDPOINT=https://gitlab.com/api/v4/groups/<group id>/projects
GIT_USERNAME=<gitlab-username>
GIT_TOKEN=<gitlap-api-access-token>
```

Group ID is number sequence show in Title Bar at group index page.

[get gitlap-api-access-token in here](https://gitlab.com/-/profile/personal_access_tokens)

refer [docker-compose.yaml](docker-compose.yaml) 